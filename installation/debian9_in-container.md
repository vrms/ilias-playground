# Installion of ILIAS inside a debian 9 Container

## create a debian 9 container and enter it

`lxc launch images:debian/9 ilias`

`lxc exec ilias -- bash`

## [get mariadb](https://linuxize.com/post/how-to-install-mariadb-on-debian-9/)

`sudo apt install software-properties-common dirmngr`

`apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8`

`sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirrors.dotsrc.org/mariadb/repo/10.2/debian stretch main'`

`apt update && apt upgrade`

`apt install mariadb-server`

check whether the installation worked with `mysql --version`

## [get php 7.3](https://computingforgeeks.com/how-to-install-php-7-3-on-debian-9-debian-8/)

`apt update && apt upgrade`

```
apt -y install lsb-release apt-transport-https ca-certificates 
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.3.list
apt install php7.3
```
check whether it has worked with ... `php --version`

## apache2.4.18

already running (I guess it came as a dependency with php)

`apt install imagemagick`


## get imagemagick

`apt install imagemagick`

